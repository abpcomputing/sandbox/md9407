import pyjapc
import numpy as np
from cmmnbuild_dep_manager import Manager
import jpype as jp
import time

mgr = Manager()
japc = pyjapc.PyJapc('', incaAcceleratorName=None)
mgr.start_jpype_jvm()
japc.rbacLogin()

cern = jp.JPackage('cern')

b1Controller = cern.lhc.concentration.client.BPMServiceLocator.getBeam1BunchTurnController()
b2Controller = cern.lhc.concentration.client.BPMServiceLocator.getBeam2BunchTurnController()
 
while True: 
	print("ARMING CONCENTRATOR")
	# specify the bunch list: the first bunch is the '0'
	b1Controller.arm('0,320,640,960,1280,1600,1920,2240,2560,2880', 10000)
	b2Controller.arm('0,320,640,960,1280,1600,1920,2240,2560,2880', 10000)
	print("TRIGGERING")
	japc.setParam('LHCMTG-GW/SendEventList', 
					{'eventNames': ['HX.BPMCAP1-CT','HX.BPMCAP2-CT'], 
					'eventPayloads': np.array([0,0], dtype=np.int32)}, checkDims=False)

	print("sleep...")
	time.sleep(5*60)
	print("wakeup")
