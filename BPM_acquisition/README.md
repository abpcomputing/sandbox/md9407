The BPMs system  of LHC ([BPMLHC FESA class](https://ccde.cern.ch/devices/classes/160/version/41580/properties)) can be triggered in bunch-by-bunch and turn-by-turn acquisition mode.
It can acquire the  B1/2 and the H/V planes.

Its buffer can record 2^17 =131'072 bunches x turns, e.g. we can record ~10 bunches x 10000 turns.

Once we trigger, the 'concentrator' will write sdds files on  
`/user/slops/data/LHC_DATA/OP_DATA/FILL_DATA/>>__FILL__NUMBER__<</BPM`

The files can be read using the [turn_by_turn](https://github.com/pylhc/turn_by_turn) python package.

To write the `sdds` files one can launch the following script in python (big thanks to Michi for the python code and Manuel and Michal for giving us an insight on their beautiful BPM system!).

```bash 
# please customize it
python run.py
```

The python distribution can be installed with acc-py.

```python
cd BPM_acquisition
source /acc/local/share/python/acc-py/pro/setup.sh
acc-py venv mypy
source mypy/bin/activate
pip install pyjapc
pip install ./bpmcap
python -m cmmnbuild_dep_manager resolve
python run.py
```
