# MD9407

Noise spectrum and emittance growth with different ADT gain.
Fill: **8957** and **8958** of 16-17 June 2022.

---
The gain settings were

1st fill **8957**, from 20230617_02h13m00s000000us to 20230617_03h26m00s000000us:
- Loop 1: 15 turns
- Loop 2: 38 turns
- Loop 3:9 4 turns
- Loop 4: 238 turns

(At 20230617_02h53m00s000000us Daniel disabled the horizontal signal from Q8 in B2 because it was saturated. Later he found that Q7 H in B1 was also at the edge. Both were fixed and re-enabled for the second fill)

2nd fill **8958**, from 20230617_05h37m00s000000us to 20230617_06h25m00s000000us:
- Loop 1: 15 turns
- Loop 2: 27 turns
- Loop 3: 47 turns
- Loop 4: 931 turns

From 20230617_06h25m00s000000us to 20230617_06h56m00s000000us:

- Loop 1: 931 turns
- Loop 2: 47 turns
- Loop 3: 27 turns
- Loop 4: 15 turns

From 20230617_06h56m00s000000us to 20230617_07h20m00s000000us:

- Loop 1: 238 turns
- Loop 2: 94 turns
- Loop 3: 38 turns
- Loop 4: 15 turns

---

Presented at [rMPP meeting on MD Block 1 2023 approval
](https://indico.cern.ch/event/1291638/contributions/5428662/attachments/2657687/4602991/2023-06-01_MDNoise.pdf) on 2 June 2023.


Data are on [eos](https://cernbox.cern.ch/s/auba0ulK2U1oUap)

`/eos/project/l/lhc-lumimod/MD9407`

together with the standard data on

- `/eos/project/l/lhc-lumimod/LuminosityFollowUp/2023/rawdata/HX:FILLN=8957` and 
- `/eos/project/l/lhc-lumimod/LuminosityFollowUp/2023/rawdata/HX:FILLN=8958`

---

# MD description

https://asm.cern.ch/md-planning/schedule/9407?schedule=LHC&year=2023&week=24


We can compute the Luminosity at https://lpc.web.cern.ch/lumiCalc.html (you can copy/paste the setting in the string below after having clicked the `retrieve/load personal setting` button).


```python
out_1_select : lumi;out_2_select : mu;in_1_select : nbch;min_1 : 1.4e11;max_1 : 1.4e11;preset_select : default;nbch : 2e11;nbb : 80;kb : 80;frev : 11245;energy : 6800;mass : 0.938;epsn : 2.1e-6;beta : 1.2;alpha : 270;dsep : 0;sigz : 0.08994;sigzunit : m;xsec : 80;runtime : 160;hubner : 0.2;
```


**MD contact persons**: Guido Sterbini, Xavier Buffat, Sofia Kostoglou

**Participants**: Daniel Valuch, Andrea Fornara, Anna Radoslavova

**MD merit**:

This MD is the combination of two experiments: - The source of '8kHz cluster' of noise lines observed in the transverse spectrum of the beam is still unknown. This experiment aims at determining the role of the ADT in the amplitude of the noises through measurements with different gain. - The ADT pickup were upgraded during the LS2 following observations of emittance growth in collision with HL-LHC-like beam-beam parameter. The reduction of the noise floor was demonstrated, this experiment aims at validating the expected improvement in term of emittance growth.

**MD Short description**:
Inject 80 equispaced individual bunches colliding in IPs 1 and 5. - Ramp - squeeze - collision - Reduce the crossing angle (within validated range) - Setup the mask ADT to act with different gains on 4 groups of 20 bunches each. - Monitor the emittance evolution for 30 min. - Depending on the beam quality, continue the experiment with different gains or re-inject a fresh beam.

**Number of hours needed:** 8

**Number of sessions:** 1


MD Coordinators Notes:
2023MD1


**Species**: Protons

**Beam phase**: Collision

**Beam**: Both

**Total number of bunches in LHC**: 80

**Beam parameters**: INDIV

**Non standard parameters**:
Higher bunch intensity: 2e11 p/b, emittance ~2um

**Filling scheme**:
525ns_80b_80_0_0_10inj8bpi

**Optics change**: No

**Orbit change**: No

**Collimation change**: No

**Changes of beam flags and interlocks**:
No


**Luminosity needed**: Yes

**Need to mask detector BCM?**: No

**What else should be changed?**:
Damper settings (mask and gain)
