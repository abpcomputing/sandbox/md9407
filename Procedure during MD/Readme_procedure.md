# Procedure for the MD9407

## Steps and checks during the MD

1. Four different ADT gains.
2. Change of the setting in the end!
3. Check the data if it is not saturated!
4. Copy the data from the circular buffer to /eos/project-l/lhc-limimod/MD9407 using the script copy_updated.py from the repository.
5. Do another fill with different ADT gains e.g. times two to be checked.

## Analysis of the data
 - Goal: Find what and how the ADT gain impacts the intensity of the 3 kHz and 8 kHz cluster (low frequency and high frequency cluster).
 - Expectation: Increased gain, lowers the intensity of the harmonics! To be proven.
 - Group the bunches with the same ADT gain and plot the spectrogram -> four different ADT gains to compare.

 For the analysis we use a modified version of [ADTObsBox_analysis](https://github.com/skostogl/ADTObsBox_analysis/tree/master/002_submit_analysis_htcondor) to be updated. With this code we can select the bunches and compare there intensity, which would tell us what the effect the transverse damper has on the harmonics (interesting for us is the 8 kHz cluster). 
