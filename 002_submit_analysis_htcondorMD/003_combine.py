import numpy as np
import glob
import pandas as pd
from pathlib import Path
import subprocess
import datetime

# Path that files will be mounted
path = 'ADTObsBox_data'

# Simplify the search of files
day = "*"  # in CET
time = "*"
beamplane = "*"  # All beamplanes
pu = "*"  # All pickups

position = 1
# Subsample
sample_files = True
if sample_files:
    sample_freq = 60  # in sec

# Fill nb
fill_nb = 8957

# Save directory for each beamplane and pickup
#save_dir = f"filenames_Fill{fill_nb}_all_beamplanes_all_pickups"

#Path(save_dir).mkdir(parents=True, exist_ok=True)


# If true, will search in fill.parquet for t1-t2. otherwise, user can define t1-t2 manually
read_fill_info = True

# File format
file_format = f"{path}/{beamplane}_{pu}/{day}/{time}/*"

if read_fill_info:
    df_modes = pd.read_parquet("fills.parquet")
    df_fills = pd.read_parquet("fills.parquet")
    if df_fills['HX:FILLN'].iloc[0] == None:
        df_fills['HX:FILLN'].iloc[0] = df_fills['HX:FILLN'].dropna().iloc[0]
    df_fills['HX:FILLN'] = df_fills['HX:FILLN'].ffill(axis=0)
    mode = df_fills['HX:BMODE'].dropna().unique()
    print(mode)
    df_current = df_fills[df_fills['HX:FILLN'] == str(fill_nb)]
    try:
        t2 = pd.Timestamp(df_current[df_current['HX:BMODE'] == 'BEAMDUMP'].index[0], tz='UTC')
    except:
        print("BEAMDUMP not found!")
        t2 = pd.Timestamp(df_current.index[-1], tz='UTC')
    t1 = pd.Timestamp(df_current[df_current['HX:BMODE'] == 'INJPHYS'].index[0], tz='UTC')
    print("INJPHYS and last mode", t1, t2)
else:
    t1 = pd.Timestamp('2023-04-23 14:00', tz='UTC')
    t2 = pd.Timestamp('2023-04-23 23:10', tz='UTC')

print(f"Time range considered (UTC): {t1}-{t2}")

def fromName2Timestamp(current_fileName, tz_start='CET', tz_end='UTC'):
    year = current_fileName.split('_')[-2][0:4]
    month = current_fileName.split('_')[-2][4:6]
    day = current_fileName.split('_')[-2][6:]
    full_datetime = f'{day}/{month}/{year} ' + current_fileName.split('_')[-1].split('.')[0]
    return pd.Timestamp(datetime.datetime.strptime(full_datetime, "%d/%m/%Y %Hh%Mm%Ss%fus")).tz_localize(tz_start, ambiguous=False).tz_convert(tz_end)

def find_pickup(current_fileName):
    return current_fileName.split('_')[-3]

def find_beamplane(current_fileName):
    return current_fileName.split('_')[-4].split("/")[-1]

# Create symbolic link
Path(path).mkdir(parents=True, exist_ok=True)
subprocess.call(f"fusermount -u {path}", shell=True)
print("Mounting NFS..")
mysymbolic_link = f"sshfs aradosla@lxplus.cern.ch:/eos/project/l/lhc-lumimod/MD9407 {path} -o IdentityFile=/afs/cern.ch/user/a/aradosla/.ssh/id_rsa"

subprocess.call(f"{mysymbolic_link}", shell=True)

print("Looking for files..")
filenames = (glob.glob(file_format))
filenames.sort(key = lambda x: fromName2Timestamp(x.split("/")[-1]))



# Create symbolic link
#Path(path).mkdir(parents=True, exist_ok=True)
#subprocess.call(f"fusermount -u {path}", shell=True)
#print("Mounting NFS..")
#subprocess.call(f"sshfs aradosla@lxplus.cern.ch:/eos/project/l/lhc-lumimod/MD9407 {path} -o IdentityFile=/afs/cern.ch/user/a/aradosla/.ssh/id_rsa", shell=True)

print("Looking for files..")
#filenames = (glob.glob(file_format))
#filenames.sort(key=lambda x: fromName2Timestamp(x.split("/")[-1]))

# Filter based on time
t1 = t1.tz_convert("CET")
t2 = t2.tz_convert("CET")
print(f"Selected files from {t1} to {t2} CET")
print("")

filenames_to_consider = np.array([i for i in filenames if (fromName2Timestamp(i.split("/")[-1]) >= t1) and (fromName2Timestamp(i.split("/")[-1]) <= t2)])
print(f"Found {len(filenames_to_consider)} files fitting criteria")

# Create a dictionary to store the sampled filenames for each beamplane and pickup
sampled_filenames = {}

# Add the following line before the loop
beamplane_to_consider = [find_beamplane(i) for i in filenames_to_consider]
# Add the following line before the loop
pus_to_consider = [find_pickup(i) for i in filenames_to_consider]


for current_beamplane in np.unique(beamplane_to_consider):
    for current_pu in np.unique(pus_to_consider):
        current_filenames = np.array([i for i in filenames_to_consider if current_beamplane in i and current_pu in i])
        print(current_filenames)
        if len(current_filenames) > 0:
            times = [fromName2Timestamp(x.split("/")[-1]) for x in current_filenames]
            sampled_times = pd.date_range(times[0], times[-1], freq=f"{sample_freq}S")
            closest_idxs = np.array([min(enumerate(times), key=lambda b: abs(time - b[1]))[0] for time in sampled_times])
            filenames_to_consider_temp = current_filenames[closest_idxs]
            # Store the sampled filenames for the current beamplane and pickup
            key = (current_beamplane, current_pu)
            if key not in sampled_filenames:
                sampled_filenames[key] = []
            sampled_filenames[key].extend(filenames_to_consider_temp)

# Save the sampled filenames for each beamplane and pickup in separate Parquet files
for (current_beamplane, current_pu), filenames in sampled_filenames.items():
    print(filenames)
    save_to = f"filenames_Fill{fill_nb}_{current_beamplane}_{current_pu}_{position}.parquet"
    pd.DataFrame({"filenames": filenames}).to_parquet(save_to)


# Unmount
subprocess.call(f"fusermount -u {path}", shell=True)

